.model small
.code
org 100h
start:
	jmp mulai
nama	    db 13,10,'Nama Anda	     : $'
hp	    db 13,10,'No. HP/Telp    : $'
lanjut	    db 13,10,'LANJUT Tekan y untuk lanjut >>>>>>>>>>>>> $'
tampung_nama	db 30,?,30 dup(?)
tampung_hp	db 13,?,13 dup(?)

a db 01
b db 02
c db 03
d db 04
e db 05
f db 06
g db 07
h db 08
i db 09
j dw 15

daftar1 db 13,10,'+-----------------------------------------------------+'
	db 13,10,'|		DAFTAR MASKER KECANTIKAN		|'
	db 13,10,'|---+---------------------------------+---------------+'
	db 13,10,'|No | Facial Treatment		| Harga		|'
	db 13,10,'+---+---------------------------------+---------------+'
	db 13,10,'|01 | Photo Facial			| Rp. 225.000,00|'
	db 13,10,'+---+---------------------------------+---------------+'
	db 13,10,'|02 | Facial Nano Colagen		| Rp. 150.000,00|'
	db 13,10,'+---+---------------------------------+---------------+'
	db 13,10,'|03 | Facial Crystal			| Rp. 180.000,00|'
	db 13,10,'+---+---------------------------------+---------------+'
	db 13,10,'|04 | Facial Ultrasound		| Rp. 250.000,00|'
	db 13,10,'+---+---------------------------------+---------------+'
	db 13,10,'|05 | Facial Oxyglow			| Rp. 250.000,00|'
	db 13,10,'+---+---------------------------------+---------------+','$'

daftar2 db 13,10,'+---+---------------------------------+---------------+'
	db 13,10,'|No | Magna Specialty			| Harga		|'
	db 13,10,'+---+---------------------------------+---------------+'
	db 13,10,'|06 | Microdermabrasi			| Rp. 300.000,00|'
	db 13,10,'+---+---------------------------------+---------------+'
	db 13,10,'|07 | 2in1 Micro-Oxy			| Rp. 400.000,00|'
	db 13,10,'+---+---------------------------------+---------------+'
	db 13,10,'|08 | 3in1 Micro-peel 		| Rp. 450.000,00|'
	db 13,10,'+---+---------------------------------+---------------+'
	db 13,10,'|09 | 3in1 Micro-peel-Oxy		| Rp. 550.000,00|'
	db 13,10,'+---+---------------------------------+---------------+'
	db 13,10,'|10 | Detox Treatment  		| Rp. 250.000,00|'
	db 13,10,'+---+---------------------------------+---------------+'
	db 13,10,'|11 | Hydrafacial	  		| Rp. 375.000,00|'
	db 13,10,'+---+---------------------------------+---------------+','$'

daftar3	db 13,10,'+---+---------------------------------+---------------+'
	db 13,10,'|No | Hair Treatment	  		| Harga		|'
	db 13,10,'+---+---------------------------------+---------------+'
	db 13,10,'|12 | Mesohair	  		| Rp. 375.000,00|'
	db 13,10,'+---+---------------------------------+---------------+'
	db 13,10,'|13 | PRP Hair	  		| Rp. 550.000,00|'
	db 13,10,'+---+---------------------------------+---------------+','$'
	
daftar4	db 13,10,'+---+---------------------------------+---------------+'
	db 13,10,'|No | Eye Treatment 		  	| Harga		|'
	db 13,10,'+---+---------------------------------+---------------+'
	db 13,10,'|14 | RF Eye		  		| Rp. 200.000,00|'
	db 13,10,'+---+---------------------------------+---------------+'
	db 13,10,'|15 | Peeling Mata		  	| Rp. 250.000,00|'
	db 13,10,'+---+---------------------------------+---------------+','$'

daftar5	db 13,10,'+---+---------------------------------+---------------+'
	db 13,10,'|No | IPL Treatment 		  	| Harga		|'
	db 13,10,'+---+---------------------------------+---------------+'
	db 13,10,'|16 | IPL ACNE		  	| Rp. 199.000,00|'
	db 13,10,'+---+---------------------------------+---------------+'
	db 13,10,'|17 | IPL Rejuvenation		| Rp. 225.000,00|'
	db 13,10,'+---+---------------------------------+---------------+','$'

daftar6	db 13,10,'+---+---------------------------------+---------------+'
	db 13,10,'|No | Mesotherapy 		  	| Harga		|'
	db 13,10,'+---+---------------------------------+---------------+'
	db 13,10,'|18 | Mesobright		  	| Rp. 450.000,00|'
	db 13,10,'+---+---------------------------------+---------------+'
	db 13,10,'|19 | Mesoacne		  	| Rp. 450.000,00|'
	db 13,10,'+---+---------------------------------+---------------+'
	db 13,10,'|20 | Mesoaging		  	| Rp. 475.000,00|'
	db 13,10,'+---+---------------------------------+---------------+'
	db 13,10,'|21 | Mesoscar		  	| Rp. 475.000,00|'
	db 13,10,'+---+---------------------------------+---------------+','$'

daftar7 db 13,10,'+---+---------------------------------+---------------+'
	db 13,10,'|No | PRP Treatment 		  	| Harga		|'
	db 13,10,'+---+---------------------------------+---------------+'
	db 13,10,'|22 | PRP for Scar Acne		| Rp. 650.000,00|'
	db 13,10,'+---+---------------------------------+---------------+'
	db 13,10,'|23 | Ejuvenation PRP			| Rp. 675.000,00|'
	db 13,10,'+---+---------------------------------+---------------+'
	db 13,10,'|TENTUKAN MASKER PILIHAN ANDA SESUAI KODE/NO PD TABEL |'
	db 13,10,'+---+---------------------------------+---------------+','$'

pilih_msk   db 13,10,'Silahkan masukkan No/kode masker yang anda pilih: $'
eror	    db 13,10,'KODE YG ANDA MASUKKAN SALAH $'
success	    db 13,10,'Selamat Anda Berhasil $'

	mulai:
	mov ah,09h
	lea dx,nama
	int 21h
	mov ah,0ah
	lea dx,tampung_nama
	int 21h
	push dx

	mov ah,09h
	mov dx,offset daftar1
	int 21h
	mov ah,09h
	mov dx,offset lanjut
	int 21h
	mov ah,01
	int 21h
	cmp al,'y'
	je page2
	jne error_msg

page2:
	mov ah,09h
	mov dx,offset daftar2
	int 21h
	mov ah,09h
	mov dx,offset lanjut
	int 21h
	mov ah,01h
	int 21h
	cmp al,'y'
	je page3
	jne error_msg

page3:
	mov ah,09h
	mov dx,offset daftar3
	int 21h
	mov ah,09h
	mov dx,offset lanjut
	int 21h
	mov ah,01h
	int 21h
	cmp al,'y'
	je page4
	jne error_msg

page4:
	mov ah,09h
	mov dx,offset daftar4
	int 21h
	mov ah,09h
	mov dx,offset lanjut
	int 21h
	mov ah,01h
	int 21h
	cmp al,'y'
	je page5
	jne error_msg

page5:
	mov ah,09h
	mov dx,offset daftar5
	int 21h
	mov ah,09h
	mov dx,offset lanjut
	int 21h
	mov ah,01h
	int 21h
	cmp al,'y'
	je page6
	jne error_msg
	
page6:
	mov ah,09h
	mov dx,offset daftar6
	int 21h
	mov ah,09h
	mov dx,offset lanjut
	int 21h
	mov ah,01h
	int 21h
	cmp al,'y'
	je page7
	jne error_msg

page7:
	mov ah,09h
	mov dx,offset daftar7
	int 21h
	mov ah,09h
	mov dx,offset lanjut
	int 21h
	mov ah,01h
	int 21h
	cmp al,'y'
	je proses
	jne error_msg

error_msg:
	mov ah,09h
	mov dx,offset error_msg
	int 21h
	int 20h

proses:
	mov ah,09h
	mov dx,offset pilih_msk
	int 21h
	mov ah,1
	int 21h
	mov bh,al
	mov ah,1
	int 21h
	mov bl,al
	
	cmp bh,'0'
	cmp bl,'1'
	je hasil1

	cmp bh,'0'
	cmp bl,'2'
	je hasil2

	cmp bh,'0'
	cmp bl,'3'
	je hasil3

	cmp bh,'0'
	cmp bl,'4'
	je hasil4

	cmp bh,'0'
	cmp bl,'5'
	je hasil5

	cmp bh,'0'
	cmp bl,'6'
	je hasil6

	cmp bh,'0'
	cmp bl,'7'
	je hasil7
	
	cmp bh,'0'
	cmp bl,'8'
	je hasil8

	cmp bh,'0'
	cmp bl,'9'
	je hasil9

	;cmp bh,'1'
	;cmp bl,'0'
	;je hasil10

	;cmp bh,'1'
	;cmp bl,'1'
	;je hasil11

	;cmp bh,'1'
	;cmp bl,'2'
	;je hasil12

	;cmp bh,'1'
	;cmp bl,'3'
	;je hasil13

	;cmp bh,'1'
	;cmp bl,'4'
	;je hasil14

	;cmp bh,'1'
	;cmp bl,'5'
	;je hasil15

	;cmp bh,'1'
	;cmp bl,'6'
	;je hasil16

	;cmp bh,'1'
	;cmp bl,'7'
	;je hasil17

	;cmp bh,'1'
	;cmp bl,'8'
	;je hasil18

	;cmp bh,'1'
	;cmp bl,'9'
	;je hasil19

	;cmp bh,'2'
	;cmp bl,'0'
	;je hasil20

	;cmp bh,'2'
	;cmp bl,'1'
	;je hasil21

	;cmp bh,'2'
	;cmp bl,'2'
	;je hasil22	

	;cmp bh,'2'
	;cmp bl,'3'
	;je hasil23

	jne error_msg
;--------------------------------------------------------

hasil1:
	mov ah,09h
	lea dx,teks1
	int 21h
	int 20h

hasil2:
	mov ah,09h
	lea dx,teks2
	int 21h
	int 20h

hasil3:
	mov ah,09h
	lea dx,teks3
	int 21h
	int 20h

hasil4:	
	mov ah,09h
	lea dx,teks4
	int 21h
	int 20h

hasil5:
	mov ah,09h
	lea dx,teks5
	int 21h
	int 20h

hasil6:
	mov ah,09h
	lea dx,teks6
	int 21h
	int 20h

hasil7:
	mov ah,09h
	lea dx,teks7
	int 21h
	int 20h

hasil8:
	mov ah,09h
	lea dx,teks8
	int 21h
	int 20h

hasil9:
	mov ah,09h
	lea dx,teks9
	int 21h
	int 20h

hasil10:
	mov ah,09h
	lea dx,teks10
	int 21h
	int 20h	

hasil11:
	mov ah,09h
	lea dx,teks11
	int 21h
	int 20h	

hasil12:
	mov ah,09h
	lea dx,teks12
	int 21h
	int 20h

hasil13:
	mov ah,09h
	lea dx,teks13
	int 21h
	int 20h	

hasil14:
	mov ah,09h
	lea dx,teks14
	int 21h
	int 20h	

hasil15:
	mov ah,09h
	lea dx,teks15
	int 21h
	int 20h	

hasil16:
	mov ah,09h
	lea dx,teks16
	int 21h
	int 20h

hasil17:
	mov ah,09h
	lea dx,teks17
	int 21h
	int 20h

hasil18:
	mov ah,09h
	lea dx,teks18
	int 21h
	int 20h

hasil19:
	mov ah,09h
	lea dx,teks19
	int 21h
	int 20h

hasil20:
	mov ah,09h
	lea dx,teks20
	int 21h
	int 20h

hasil21:
	mov ah,09h
	lea dx,teks21
	int 21h
	int 20h

hasil22:
	mov ah,09h
	lea dx,teks22
	int 21h
	int 20h

hasil23:
	mov ah,09h
	lea dx,teks23
	int 21h
	int 20h	


;--------------------------------------------------------

teks1	db 13,10,'Tipe kulit Anda : Acne'
	db 13,10,'Anda memilih Photo Facial'
	db 13,10,'Total harga yang harus Anda bayar adalah RP. 225.000,00'
	db 13,10,'Terima Kasih $'

teks2	db 13,10,'Tipe kulit Anda : Acne'
	db 13,10,'Anda memilih Facial Nano Colagen'
	db 13,10,'Total harga yang harus Anda bayar adalah RP. 150.000,00'
	db 13,10,'Terima Kasih $' 

teks3	db 13,10,'Tipe kulit Anda : Acne'
	db 13,10,'Anda memilih Facial Crystal'
	db 13,10,'Total harga yang harus Anda bayar adalah RP. 180.000,00'
	db 13,10,'Terima Kasih $' 

teks4	db 13,10,'Tipe kulit Anda : Acne'
	db 13,10,'Anda memilih Facial Ultrasound'
	db 13,10,'Total harga yang harus Anda bayar adalah RP. 250.000,00'
	db 13,10,'Terima Kasih $'

teks5	db 13,10,'Tipe kulit Anda : Acne'
	db 13,10,'Anda memilih Facial Oxyglow'
	db 13,10,'Total harga yang harus Anda bayar adalah RP. 250.000,00'
	db 13,10,'Terima Kasih $'

teks6	db 13,10,'Tipe kulit Anda : Acne'
	db 13,10,'Anda memilih Microdermabrasi'
	db 13,10,'Total harga yang harus Anda bayar adalah RP. 300.000,00'
	db 13,10,'Terima Kasih $'

teks7	db 13,10,'Tipe kulit Anda : Acne'
	db 13,10,'Anda memilih Micro-Oxy'
	db 13,10,'Total harga yang harus Anda bayar adalah RP. 400.000,00'
	db 13,10,'Terima Kasih $'

teks8	db 13,10,'Tipe kulit Anda : Acne'
	db 13,10,'Anda memilih 3in1 Micro-peel'
	db 13,10,'Total harga yang harus Anda bayar adalah RP. 450.000,00'
	db 13,10,'Terima Kasih $'

teks9	db 13,10,'Tipe kulit Anda : Acne'
	db 13,10,'Anda memilih 3in1 Micro-peel-Oxy'
	db 13,10,'Total harga yang harus Anda bayar adalah RP. 550.000,00'
	db 13,10,'Terima Kasih $'

teks10	db 13,10,'Tipe kulit Anda : Acne'
	db 13,10,'Anda memilih Detox Treatment'
	db 13,10,'Total harga yang harus Anda bayar adalah RP. 250.000,00'
	db 13,10,'Terima Kasih $'

teks11	db 13,10,'Tipe kulit Anda : Acne'
	db 13,10,'Anda memilih Hydrafacial'
	db 13,10,'Total harga yang harus Anda bayar adalah RP. 375.000,00'
	db 13,10,'Terima Kasih $'

teks12	db 13,10,'Tipe kulit Anda : Acne'
	db 13,10,'Anda memilih Mesohair'
	db 13,10,'Total harga yang harus Anda bayar adalah RP. 375.000,00'
	db 13,10,'Terima Kasih $'

teks13	db 13,10,'Tipe kulit Anda : Acne'
	db 13,10,'Anda memilih PRP Hair'
	db 13,10,'Total harga yang harus Anda bayar adalah RP. 550.000,00'
	db 13,10,'Terima Kasih $'

teks14	db 13,10,'Tipe kulit Anda : Acne'
	db 13,10,'Anda memilih RF Eye'
	db 13,10,'Total harga yang harus Anda bayar adalah RP. 200.000,00'
	db 13,10,'Terima Kasih $'

teks15	db 13,10,'Tipe kulit Anda : Acne'
	db 13,10,'Anda memilih Peeling Mata'
	db 13,10,'Total harga yang harus Anda bayar adalah RP. 250.000,00'
	db 13,10,'Terima Kasih $'

teks16	db 13,10,'Tipe kulit Anda : Acne'
	db 13,10,'Anda memilih IPL ACNE'
	db 13,10,'Total harga yang harus Anda bayar adalah RP. 199.000,00'
	db 13,10,'Terima Kasih $'

teks17	db 13,10,'Tipe kulit Anda : Acne'
	db 13,10,'Anda memilih IPL Rejuvenation'
	db 13,10,'Total harga yang harus Anda bayar adalah RP. 225.000,00'
	db 13,10,'Terima Kasih $'

teks18	db 13,10,'Tipe kulit Anda : Acne'
	db 13,10,'Anda memilih Mesobright'
	db 13,10,'Total harga yang harus Anda bayar adalah RP. 450.000,00'
	db 13,10,'Terima Kasih $'

teks19	db 13,10,'Tipe kulit Anda : Acne'
	db 13,10,'Anda memilih Mesoacne'
	db 13,10,'Total harga yang harus Anda bayar adalah RP. 450.000,00'
	db 13,10,'Terima Kasih $'

teks20	db 13,10,'Tipe kulit Anda : Acne'
	db 13,10,'Anda memilih Mesoaging'
	db 13,10,'Total harga yang harus Anda bayar adalah RP. 475.000,00'
	db 13,10,'Terima Kasih $'

teks21	db 13,10,'Tipe kulit Anda : Acne'
	db 13,10,'Anda memilih Mesoscar'
	db 13,10,'Total harga yang harus Anda bayar adalah RP. 475.000,00'
	db 13,10,'Terima Kasih $'

teks22	db 13,10,'Tipe kulit Anda : Acne'
	db 13,10,'Anda memilih PRP for Scar Acne'
	db 13,10,'Total harga yang harus Anda bayar adalah RP. 650.000,00'
	db 13,10,'Terima Kasih $'

teks23	db 13,10,'Tipe kulit Anda : Acne'
	db 13,10,'Anda memilih Ejuvenation PRP'
	db 13,10,'Total harga yang harus Anda bayar adalah RP. 675.000,00'
	db 13,10,'Terima Kasih $'

end start